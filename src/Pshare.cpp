#include "../lib/Pshare.h"

using namespace std;

void pshare_P(const char *TraceFilePath, int s, int gh, int ph){

    // VARIABLES

/////////////       int Variables         ////////////////////////////////////////
    int counter = 0;                                                // temp variable, current state of the counter, taken from BHT
    int bits = pow(2,s);                                            // size 2^s bits
    int index;                                                      // index of BHT
    double bcount = 0;                                                 // branch counter
    int Tbc = 0;                                                    // statdistic variable, Taken branches Correct
    int Tbi = 0;                                                    // statdistic variable, Taken branches Incorrect
    int Nbc = 0;                                                    // statdistic variable, Not Taken branches Correct
    int Nbi = 0;                                                    // statdistic variable, Not Taken branches InCorrect
    int pht = 0;                                                    // Global History table register
    int mask_PHT = pow(2,ph);                                       // mask to keep the pht size on gh

//////////       String Variables         ////////////////////////////////////////////
    string prediction;                                              // prediction
    string output = "gunzip -c ./branch-trace-gcc.trace.gz | ./src/branch\n Debug on Gshare branch predictor\n";       // Output results Log string, concatenated results..
    string PC, Outcome;                                             // temp variables to save from the Trace
    vector<int> BHT (bits,0);                                       // 2^s ints counters with value 00
    vector<int> PHT (bits,0);                                       // 2^s ints counters with value 00

//////////       TRACE file openned to read   ////////////////////////////////////////////
    FILE *f;
    f = fopen(TraceFilePath,"r+");                                          // Opening the file on Read mode
    if (f == NULL) {                                                        // open error handling
        cout << "Can't open the file.\n"<< endl;
        exit(EXIT_FAILURE);
    }
    while ( !(!feof(f)^(bcount < 200) )){                                   // traversing the file to the limit
        readTraceLine(f, PC, Outcome);                                      // Read one line of the trace, getting PC & Outcome from file.
        index = LSB(PC, s);                                                  // getting the index( -s LSB of the PC)
//////////////////// first stage PC index to BHT   //////////////////////////////
       pht = PHT[index];                // getting a PHT entry
       int indexBHT = index^pht;            // XOR 
///////////////////////////////////////////////////////////////////////////
        prediction = getPred(indexBHT, BHT);                                   // getting the prediction, actual state of the BHT index
        learning(indexBHT, Outcome, prediction, BHT, BHT[indexBHT], 3);           // Update the BHT entry, Counters training
         if (Outcome=="T"){                                                    // Update pht with shift left 1 (Taken) or 0 (Not taken)
            pht = 2*pht + 1;                                        // << 1
            }
        else{
            pht = 2*pht;                                            // << 0;
            }
        if( pht > mask_PHT-1) {
            pht = pht - mask_PHT;
        }

        PHT[index] = pht;                // updating the PHT entry
        contadores(prediction,Outcome,bcount,Tbc,Tbi,Nbc,Nbi);              // Update stadistic counters
        output = out(BHT, output, PC, Outcome, prediction);                      //  Update output string to create a output log file with a resume of the results
        }
    makeTable(output, "./results/PshareResults.log");                                    // make the output results log table.
    console("Pshare", bits, gh, ph, Tbc, Tbi, Nbc, Nbi);                        // print the result on console
    rewind(f);
    fclose(f);
}