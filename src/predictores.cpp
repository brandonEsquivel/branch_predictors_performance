/*#include "funciones.cpp"

using namespace std;


int Gshare(string &PC,string &outcome,int &GHR, vector<int> &BHT, int s,int gh, string &add){
    
    // VARIABLES 
    int counter,updateHR,bgh,addr1,addr2;                            // Variables auxiliares enteras, contadores y direcciones
    int bits = pow(2,s);                                            // Obtengo size 2^s bits
    bgh =  pow(2,gh);                                               // Obtengo size 2^gh del GHT
    string pred;                                                    // Creando variables auxiliares string
    
    //LECTURA Y CONVERSION                                    
    addr1 = LSB_PC(PC,bits);                                                    // Extrayendo bits mas significativos de la PC
    addr2 = addr1^GHR;                                                          // Realizando Xor  
    counter = BHT[addr2];                                                       // Obteniendo contador actual
    pred = getPred(addr2,BHT);                                                  // Obteniedo prediccion                                             

    //ACTUALIZACION Y ENTRENAMIENTO
    if( outcome == "T"){ updateHR=1; }
    else{                updateHR=0; }
    learning(addr2,outcome,pred,BHT,counter);                               //  actualizo BHT
    shiftleft(GHR, bgh, bits, updateHR);                            //  actualizo PHR
    add = out(add,PC,outcome,pred);
    if(pred=="T"){  return 1;   }
    else{           return 0;   }

}




int Pshare(string &PC,string &outcome, vector<int> &PHR, vector<int> &BHT,  int s,int ph, string &add){
    
    // VARIABLES 
    int counter,updateHR,bph,addr1,addr2;                    // Variables auxiliares enteras, contadores y direcciones
    int bits = pow(2,s);                                            // Obtengo size 2^s bits
    bph =  pow(2,ph);                                               // Obtengo size 2^ph del PHT

    //LECTURA Y CONVERSION 
    addr1 = LSB_PC(PC,bits);                                                    // Extrayendo bits mas significativos de la PC
    addr2 = addr1^PHR[addr1];                                                   // Realizando Xor  
    counter = BHT[addr2];                                                       // Obteniendo contador actual
    string pred = getPred(addr2,BHT);                                                  // Obteniedo prediccion                                             

    //ACTUALIZACION Y ENTRENAMIENTO
    if( outcome == "T"){ updateHR=1; }
    else{                updateHR=0; }
    learning(addr2,outcome,pred,BHT,counter);                               //  actualizo BHT
    shiftleft(PHR[addr1], bph, bits, updateHR);                            //  actualizo PHR
    add = out(add,PC,outcome,pred);
    if(pred=="T"){  return 1;   }
    else{           return 0;   }

}


int Tournament(FILE *f, int s, int gh, int ph, string &add1, string &add, int o){
    double bcount,Tbc,Tbi,Nbc,Nbi, addr,predGs, predPs, counter;
    int bits = pow(2,s);
    string outcome, PC;

 // INICIALIZANDO VALORES
    int GHR = 0;
    bcount = 0;
    Tbc = 0;
    Tbi = 0;
    Nbc= 0;
    Nbi= 0;
    addr = 0;
    predGs= 0; 
    predPs = 0;
    counter = 0;

    vector<int> BHT(bits,0);
    vector<int> PHR(bits,0);
    vector<int> Metapred(bits,0);                                                 //  iniciando el metapredictor en el estado strongly prefer pshared
    char aux [80];

    
    // misma logica de bucle, con su respectiva leraning y llamado a los predictores necesarios
    while ( !(!feof(f)^(bcount < 200) )) 
    {
        fscanf(f,"%s",aux);
        PC = aux;
        fscanf(f,"%s",aux);
        outcome = aux;

        addr = LSB_PC(PC,bits);
        
        string predPss, predGss, Salida;

        predPs = Pshare(PC,outcome,PHR,BHT,s,ph,add); if(predPs == 0){predPss = "N";} else{ predPss = "T";}
        predGs = Gshare(PC,outcome,GHR,BHT,s,gh,add); if(predGs == 0){predGss = "N";} else{ predGss = "T";}

    // logica de entrenamiento del metapredictor
    
        if( Metapred[addr] < 2){  
            add1 = out(add1,PC,outcome,predPss);
            contadores(predPss,outcome,bcount,Tbc,Tbi,Nbc,Nbi);
          }
          else
          {   
              add1 = out(add1,PC,outcome,predGss);
              contadores(predGss,outcome,bcount,Tbc,Tbi,Nbc,Nbi);
          }
          
        // ENTRENAMIENTO
        
        
        if(  (predPs==1 && outcome == "T" && predGs==0)   ){
            
            if(Metapred[addr]>0) {Metapred[addr]--; }
        }

        if ( (predGs==1 && outcome == "T" && predPs==0) ){
            if(Metapred[addr]<3) {Metapred[addr]++; }
        }

        if(  (predPs==1 && outcome == "N" && predGs==0)   ){
             if(Metapred[addr]<3) {Metapred[addr]++;  }
        }
        
        if ( (predGs==1 && outcome == "N" && predPs==0) ){
                if(Metapred[addr]>0) {Metapred[addr]--; }
        }
}
    
     salidaTour(f,o,gh,ph,add1,bits,Tbc,Tbi,Nbc,Nbi);               // logica de salida
return 0;
}*/