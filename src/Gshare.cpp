#include "../lib/Gshare.h"

using namespace std;

void gshare_P(const char *TraceFilePath, int s, int gh, int ph){

    // VARIABLES

/////////////       int Variables         ////////////////////////////////////////
    int counter = 0;                                                // temp variable, current state of the counter, taken from BHT
    int bits = pow(2,s);                                            // size 2^s bits
    int index;                                                      // index of BHT
    double bcount = 0;                                                 // branch counter
    int Tbc = 0;                                                    // statdistic variable, Taken branches Correct
    int Tbi = 0;                                                    // statdistic variable, Taken branches Incorrect
    int Nbc = 0;                                                    // statdistic variable, Not Taken branches Correct
    int Nbi = 0;                                                    // statdistic variable, Not Taken branches InCorrect
    int GHT = 0;                                                    // Global History table register
    int mask_GHT = pow(2,gh);                                       // mask to keep the GHT size on gh

//////////       String Variables         ////////////////////////////////////////////
    string prediction;                                              // prediction
    string output = "gunzip -c ./branch-trace-gcc.trace.gz | ./src/branch\n Debug on Gshare branch predictor\n";       // Output results Log string, concatenated results..
    string PC, Outcome;                                             // temp variables to save from the Trace
    vector<int> BHT (bits,0);                                       // 2^s ints counters with value 00

//////////       TRACE file openned to read   ////////////////////////////////////////////
    FILE *f;
    f = fopen(TraceFilePath,"r+");                                          // Opening the file on Read mode
    if (f == NULL) {                                                        // open error handling
        cout << "Can't open the file.\n"<< endl;
        exit(EXIT_FAILURE);
    }
    while ( !(!feof(f)^(bcount < 200) )){                                   // traversing the file to the limit
        readTraceLine(f, PC, Outcome);                                      // Read one line of the trace, getting PC & Outcome from file.
        index = LSB(PC, s);                                                  // getting the index( -s LSB of the PC)
//////////////////// first stage PC index to BHT   //////////////////////////////
        int indexBHT = index^GHT;
///////////////////////////////////////////////////////////////////////////
        prediction = getPred(indexBHT, BHT);                                   // getting the prediction, actual state of the BHT index
        learning(indexBHT, Outcome, prediction, BHT, BHT[indexBHT], 3);           // Update the BHT entry, Counters training
         if (Outcome=="T"){                                                    // Update GHT with shift left 1 (Taken) or 0 (Not taken)
            GHT = 2*GHT + 1;                                        // << 1
            }
        else{
            GHT = 2*GHT;                                            // << 0;
            }
        if( GHT > mask_GHT-1) {
            GHT = GHT - mask_GHT;
        }

       // GHT &= mask_GHT;                                                   // mask
        contadores(prediction,Outcome,bcount,Tbc,Tbi,Nbc,Nbi);              // Update stadistic counters
        output = out(BHT, output, PC, Outcome, prediction);                      //  Update output string to create a output log file with a resume of the results
        }
    makeTable(output, "./results/GshareResults.log");                                // make the output results log table.
    console("Gshare", bits, gh, 0, Tbc, Tbi, Nbc, Nbi);                     // print the result on console
    rewind(f);
    fclose(f);
}