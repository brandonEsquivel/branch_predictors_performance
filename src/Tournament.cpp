#include "../lib/Tournament.h"

using namespace std;

string outT();

void tournament_P(const char *TraceFilePath, int s, int gh, int ph ){

    // VARIABLES

/////////////       int Variables         ////////////////////////////////////////
    int counter = 0;                                                // temp variable, current state of the counter, taken from BHT
    int bits = pow(2,s);                                            // size 2^s bits
    int index;                                                      // index of BHT
    double bcount = 0;                                              // branch counter
    int Tbc = 0;                                                    // statdistic variable, Taken branches Correct
    int Tbi = 0;                                                    // statdistic variable, Taken branches Incorrect
    int Nbc = 0;                                                    // statdistic variable, Not Taken branches Correct
    int Nbi = 0;                                                    // statdistic variable, Not Taken branches InCorrect
    int GHT = 0;                                                    // Global History table register
    int mask_GHT = pow(2,gh);                                       // mask to keep the GHT size on gh
    int pht = 0;                                                    // Private History table register
    int mask_PHT = pow(2,ph);                                       // mask to keep the pht size on ph
    int metapredictor = 3;                                          // metarpedictor starts in strong prefer Pshare (11)

//////////       String Variables         ////////////////////////////////////////////
    string predictionGshare;                                              // prediction of Gshare
    string predictionPshare;                                              // prediction of Pshare
    string prediction;                                                    // final prediction of Tournament
    string predictor;                                                     // Name of the used predictor G or P.
    string output = "gunzip -c ./branch-trace-gcc.trace.gz | ./src/branch\n Debug on Tournament branch predictor\n";       // Output results Log string, concatenated results..
    string PC, Outcome;                                                   // temp variables to save from the Trace
    vector<int> PHT (bits,0);                                             // private history table
    vector<int> BHTg (bits,0);                                            // 2^s ints counters with value 00
    vector<int> BHTp (bits,0);                                            // 2^s ints counters with value 00




//////////       TRACE file openned to read   ////////////////////////////////////////////
    FILE *f;
    f = fopen(TraceFilePath,"r+");                                          // Opening the file on Read mode
    if (f == NULL) {                                                        // open error handling
        cout << "Can't open the file.\n"<< endl;
        exit(EXIT_FAILURE);
    }
    while ( !(!feof(f)^(bcount < 200) )){                                   // traversing the file to the limit
        readTraceLine(f, PC, Outcome);                                      // Read one line of the trace, getting PC & Outcome from file.
        index = LSB(PC, s);                                                  // getting the index( -s LSB of the PC)


////////////        FOR GSHARE       //////////////
//////////////////// first stage PC index to BHT   //////////////////////////////
        int indexBHTg = index^GHT;
        predictionGshare = getPred(indexBHTg, BHTg);                                         // getting the prediction, actual state of the BHT index
        learning(indexBHTg, Outcome, predictionGshare, BHTg, BHTg[indexBHTg], 3);            // Update the BHT entry, Counters training

        if (Outcome=="T"){                                                                  // Update GHT with shift left 1 (Taken) or 0 (Not taken)
            GHT = 2*GHT + 1;                                                                 // << 1
            }
        else{
            GHT = 2*GHT;                                                                     // << 0;
            }
        if( GHT > mask_GHT-1) {                                                              // keeping the result on the limit
            GHT = GHT - mask_GHT;
        }  // end if





////////////////////////////// for Pshare ////////////////////////////////////////


        pht = PHT[index];                                                                    // getting a PHT entry
        int indexBHTp = index^pht;                                                           // XOR
        predictionPshare = getPred(indexBHTp, BHTp);                                               // getting the prediction, actual state of the BHT index
        learning(indexBHTp, Outcome, predictionPshare, BHTp, BHTp[indexBHTp], 3);                  // Update the BHT entry, Counters training

        if (Outcome=="T"){                                                                   // Update pht with shift left 1 (Taken) or 0 (Not taken)
            pht = 2*pht + 1;                                                                 // << 1
            }
        else{
            pht = 2*pht;                                                                     // << 0;
            }
        if( pht > mask_PHT-1) {                                                              // keeping the result on the limit
            pht = pht - mask_PHT;
        }

        PHT[index] = pht;                                                                    // updating the PHT entry





/////////////////////////////////////////////////////



///////////////// TOURNAMENT PREDICTOR LOGIC  //////////////////

        if(metapredictor<2){
            prediction = predictionGshare;
            predictor = 'G';
        } else {
            prediction = predictionPshare;
            predictor = 'P';
        }





    contadores(prediction,Outcome,bcount,Tbc,Tbi,Nbc,Nbi);                       // Update stadistic counters
    output = outT(predictor, output, PC, Outcome, prediction);                        //  Update output string to create a output log file with a resume of the results

    //////////////////////  METAPREDICTOR UPDATE LOGIC    //////////////////////////////////////////////////////////
    if( (predictionGshare == Outcome) && (predictionPshare==Outcome)){
        metapredictor = metapredictor;
    }else if ((predictionGshare != Outcome) && (predictionPshare != Outcome))
    {
         metapredictor =  metapredictor;
    }
    else if ((predictionGshare == Outcome) && (predictionPshare != Outcome))
    {   if ( metapredictor > 0 )
        {
        metapredictor--;
        } else{
            metapredictor = 0;
        }
    } else if ((predictionGshare != Outcome) && (predictionPshare == Outcome))
    {
        if (metapredictor < 3)
        {
            metapredictor++;
        }else
        {
            metapredictor = 3;
        }
    }
  }  // end while

        console("Tournament", bits, gh, ph, Tbc, Tbi, Nbc, Nbi);                     // print the result on console
        makeTable(output, "./results/TournamentResults.log");                                    // make the output results log table.
        rewind(f);
        fclose(f);
      



} // end 
