#include "../lib/bimodal.h"
#include "../lib/Pshare.h"
#include "../lib/Gshare.h"
#include "../lib/Tournament.h"



using namespace std;

// Arguments
/*
1. Size of the BTH table. (-s)
2. Type of prediction (-bp)
3. Size of the global prediction record (-gh)
4. Size of private history records (-ph)
The program is able to be executed as follows:

gunzip -c branch-trace-gcc.trace.gz | branch -s < # > -bp < # > -gh < # > -ph < # >
*/

int main(int argc, char *argv[]){

// Input arguments, VAR init
    int s  = 0;
    int bp = 0;
    int gh = 0;
    int ph = 0;
// variables argumentos de entrada int: bits de tamano, bits de global history, bits de private history, out ON/OFF, branch predictor

//COMPROBACION DE ARGUMENTOS DE ENTRADA
    if(argc > 11){  cout << "Número de argumentos incorrecto. Verifique." << endl; exit(1); }
    if(argc < 3){   cout << "Muy pocos argumentos de entrada. " << endl; exit(1); }
    else
    {
        for (int i = 1; i < argc-1; i=i+2)          // obteniendo variables ingresadas por linea de comandos
    {
            string index = argv[i];
            if(index == "-s"){ s = atoi(argv[i+1]);}
            else if(index == "-bp"){    bp = atoi(argv[i+1]);}
            else if(index == "-gh"){    gh = atoi(argv[i+1]);    }
            else if(index == "-ph"){    ph = atoi(argv[i+1]);}
            else
            {
            cout << "El argumento de entrada  [  "<< argv[i] << " ] no es una opcion valida. Verifique las Flags disponibles." << endl; 
            exit(1);
            }
        }
    }
   /// this is the Decision Logic to select the predictor
    if (bp == 0){ // Bimodal
      bimodal_P("./src/branch", s, gh, ph);
      return 0;
    }
    else if (bp == 1){ // private
      pshare_P("./src/branch", s, gh, ph);
      return 0;
    }

    else if (bp == 2){ // global
      gshare_P("./src/branch", s, gh, ph);
      return 0;
    }

    else if (bp == 3){ // tournament
      tournament_P("./src/branch", s, gh,  ph);
      return 0;
    }

    else {
      cout << "\nWrong option, the possible options for -bp Are:  0 = Bimodal, 1 = Private, 2 = Global, 3 = Tournament. Please Try again.\n";
    }


    return 0;
}