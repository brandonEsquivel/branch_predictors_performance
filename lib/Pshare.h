#pragma once
#include "../include/header.h"
#include "../include/funciones.h"

using namespace std;

/** @brief Implementation of the bimodal predictor using the functions.cpp library
    *
    * It is the simplest dynamic predictor. It is basically an array of 2-bit counters, where each counter can take one of the following values:
    * [Strongly taken. Weakly taken, Weakly not taken, Strongly not taken].The size of the array is derived from the "-s" option, the number of inputs in the array is 2s. To make a prediction, a counter is selected from the table using the last n bits of the program counter (in this case the value of n is given by s). All counters are initialized in the strongly not taken state.
    @author Brandon ESquivel
    @date September 2020
    */


  /** Call to Pshare Predictor
     * @param const char *TraceFilePath "./Trace file path"
     * @param int s input parameter to set the BHT size
     * @return a output file  & print the results of the prediction in console*/
    void pshare_P(const char *TraceFilePath, int s, int gh, int ph );