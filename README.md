# Branch Predictors Performance

## University of Costa Rica
## September, 2020


## Integrants
1. Esquivel Molina, Brandon B52571



## Description:
 Implementation to analyze the Performance of different Branch prediction schemes through simulations in high-level language. The program reads a branch trace file and predicts the results.

Four dynamic branch predictors are implemented:

Note: All counters are initialized in the strongly not taken state.

## Bimodal Predictor (-bp 0)
It is the simplest dynamic predictor. It is basically an array of 2-bit counters, where each counter can take one of the following values: [Strongly taken, Weakly taken, Weakly not taken, Strongly not taken]. The size of the array is derived from the "-s" option, the number of inputs in the array is 2s. To make a prediction, a counter is selected from the table using the last n bits of the program counter (in this case the value of n is given by s).

## Predictor with private history (-bp 1)
This predictor is very similar to the previous one, but instead of having a single record with the global history, it has a whole table with the history of each jump. Both the size of the BHT and the PHT are given by the parameter "-s". The number of bits of history that are stored in each entry of the table is given by the option "-ph". To index the array of counters, the last n bits are used.

## Predictor with global history (-bp 2)
This predictor uses an array of size 2s, where 2-bit counters that indicate the prediction are stored, and a register with the history of the last jumps, the amount of bits in this register is given by the option "-gh" . To make the prediction, the last n bits resulting from the XOR operation between the program counter and the history register are used.

## Predictor with tournament (-bp 3)
For this metapredictor the predictor with private history and the predictor with global history are used. The two predictors are accessed in parallel and each generates a prediction independently. To choose between both predictions, use a meta predictor that indicates which of the two predictions to use. The metapredictor is a 2s size structure with 2-bit counters indicating the predictor to use. The meta predictor is initialized in the strongly prefer pshared state.


## How to run

--[ make run ]--


in the root folder of the project.

--make clean-- for remove .bin and others innecessary files

Change the arguments inside makefile for differents test.

Detailed automated build comments can be found in the makefile.